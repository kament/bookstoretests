﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace BookStoreTests
{
    [TestClass]
    public class LoginTests
    {
        private IWebDriver _driver;

        [TestMethod]
        public void ShouldLoginWithCorrectUsernameAndPasswordInFirefox()
        {
            _driver = new FirefoxDriver();
            TestSuccessLogin();
        }

        [TestMethod]
        public void ShouldLoginWithCorrectUsernameAndPasswordInChrome()
        {
            _driver = new ChromeDriver();
            TestSuccessLogin();
        }

        [TestMethod]
        public void ShouldDisplayCorrectMessageAfterFailedLoginFirefox()
        {
            _driver = new FirefoxDriver();
            TestFailLogin();
        }

        [TestMethod]
        public void ShouldDisplayCorrectMessageAfterFailedLoginChrome()
        {
            _driver = new ChromeDriver();
            TestFailLogin();
        }

        [TestMethod]
        public void ShouldRedirectToHomePageAfterUserRegistrationFireFox()
        {
            _driver = new FirefoxDriver();
            RegisterUser();
        }

        [TestMethod]
        public void ShouldRedirectToHomePageAfterUserRegistrationChrome()
        {
            _driver = new ChromeDriver();
            RegisterUser();
        }
        
        [TestMethod]
        public void ShouldShowBookDetailsFirefox()
        {
            _driver = new FirefoxDriver();
            ShowBookDetails();
        }

        [TestMethod]
        public void ShouldShowBookDetailsChrome()
        {
            _driver = new ChromeDriver();
            ShowBookDetails();
        }
        
        [TestCleanup]
        public void OnTestFinish()
        {
            _driver.Quit();
        }

        private void TestFailLogin()
        {
            TestLogin("admin", "wrongpassword", "Login_message", "Login or Password is incorrect.");
        }

        private void TestSuccessLogin()
        {
            TestLogin("admin", "admin", "Member_holder", "User Information");
        }

        private void TestLogin(string userName, string password, string elementToCheck, string assertMessage)
        {
            _driver.Url = "http://books.tto.bg/Login.aspx";

            IWebElement userNameElement = _driver.FindElement(By.Name("Login_name"));
            IWebElement passwordElement = _driver.FindElement(By.Name("Login_password"));

            userNameElement.SendKeys(userName);
            passwordElement.SendKeys(password);

            IWebElement submitButton = _driver.FindElement(By.Name("Login_login"));

            submitButton.Click();

            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(60));
            IWebElement memberHolder = wait.Until(ExpectedConditions.ElementExists(By.Id(elementToCheck)));

            Assert.IsTrue(memberHolder.Text.Contains(assertMessage));
        }

        private void RegisterUser()
        {
            _driver.Url = "http://books.tto.bg/Registration.aspx";

            IWebElement userName = _driver.FindElement(By.Name("Reg_member_login"));
            IWebElement password = _driver.FindElement(By.Name("Reg_member_password"));
            IWebElement confirmPassword = _driver.FindElement(By.Name("Reg_member_password2"));
            IWebElement firstName = _driver.FindElement(By.Name("Reg_first_name"));
            IWebElement lastName = _driver.FindElement(By.Name("Reg_last_name"));
            IWebElement email = _driver.FindElement(By.Name("Reg_email"));
            IWebElement address = _driver.FindElement(By.Name("Reg_address"));
            IWebElement phone = _driver.FindElement(By.Name("Reg_phone"));


            string userNameValue = Guid.NewGuid().ToString().Substring(0, 5);
            userName.SendKeys(userNameValue);
            password.SendKeys("test123");
            confirmPassword.SendKeys("test123");
            firstName.SendKeys("Hero");
            lastName.SendKeys("Hero");
            address.SendKeys("address");
            phone.SendKeys("4234234234");
            email.SendKeys($"test@test{userNameValue}.com");

            IWebElement registerButton = _driver.FindElement(By.Name("Reg_insert"));
            registerButton.Click();

            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(60));
            bool registerrSucceeded = wait.Until(ExpectedConditions.UrlMatches("http://books.tto.bg/Default.aspx?"));

            Assert.IsTrue(registerrSucceeded);
        }

        private void ShowBookDetails()
        {
            TestSuccessLogin();
            _driver.Url = "http://books.tto.bg/Default.aspx";

            IWebElement book = _driver.FindElement(By.Id("Recommended_holder")).FindElements(By.TagName("table"))[0].FindElement(By.TagName("a"));

            book.Click();

            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(60));
            IWebElement bookDetails = wait.Until(ExpectedConditions.ElementExists(By.Id("DetailForm_Title")));

            Assert.IsTrue(bookDetails.Text.Contains("Book Detail"));
        }
    }
}
